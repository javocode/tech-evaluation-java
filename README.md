![https://www.utec.edu.pe/sites/default/files/logo_0.png](http://www.utec.edu.pe/sites/default/files/logo_utec_.png)

# Evaluación Técnica

El objetivo de esta evaluación es validar los skills de desarrollo que el area solicita.

# Pre requisitos

* Tener instalado el Eclipse
* Tener instalado un cliente GIT
* Tener una cuenta en bitbucket
* Tener una base de datos local (mysql)

# Instrucciones

- La prueba tiene 2 partes.
- La prueba tiene una duración de 60 minutos.

---
# PARTE 01

## Definicion

Desarrollar un api rest con un endpoint que permita ejecutar sentencias sql DDL. 

## Base Url

 `http://host:port/db-util-api/`

## Endpoint

 `ddl`

## Metodo

  `POST`

## Parámetros Url

No aplica

## Parámetros BODY

```sql
CREATE TABLE `user` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8
```

## Respuesta Exitosa

```json
{
  "status": 200,
  "message": "OK"
}
```

## Respuesta Erronea

```json
{
  "status": 500,
  "message": "Error: ......"
}
```


---
# PARTE 02

## Definicion

Dado el siguiente xml, desarrollar una aplicacion java transforme cada test en una invocacion http
usan algun framework como spring rest template, apache http, etc

- La invocacion debe de ser secuencial en el orden que indica el xml.
- En cada invocacion debe pintarse por consola la respuesta del servicio
- La respuesta de una invocacion puede ser el request de la siguiente invocacion (Plus)



```xml
<?xml version="1.0"?>
<tests>
   <test id="loginEndpoint" name="login endpoint">
      <description>endpoint para loguearse</description>
      <url>http://localhost:5000/login</url>
      <method>GET</method>
      </body>
      <parameters>
        <parameter key="user">jon</parameter>
        <parameter key="password">doe</parameter>
      </parameters>
   </test>
   <test id="101" name="get user info">
      <description>regresa la info del usuario</description>
      <url>http://localhost:5000/info</url>
      <method>GET</method>
      </body>
      <parameters>
        <parameter key="token">$loginEndpointResponse.token</parameter>
      </parameters>
   </test>
</tests>
```

Los endpoints para la prueba estan en:

- https://github.com/jrichardsz/nodejs-express-snippets/blob/master/06-secuencial-endpoints.js
- Es una app simple nodejs la cual debera ser iniciada. Si es iniciada correctamente se podra consumir con :

  - http://localhost:5000/login
  - http://localhost:5000/info


# Criterios de evaluacion


* Uso de Java 1.8
* Uso de Maven
* Uso de Spring boot
* Calidad del codigo.
* Legibilidad del codigo.
* Configuracion adicionales necesarias para levantar el proyecto , ponerlas en el readme.md
* El api debe iniciar usando el comando: mvn spring-boot:run
	
	
