package com.utec.parte1.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJdbcTest
public class DataDefinitionDAOTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private DataDefinitionDAO dataDefinitionDAO;

    @Before
    public void setUp() {
        dataDefinitionDAO = new DataDefinitionDAO(jdbcTemplate);
    }

    @Test
    public void giveASqlString_thenCreateTable() {
        String sentence = "CREATE TABLE user (\n" +
                "  id int(11) DEFAULT NULL,\n" +
                "  name varchar(45) DEFAULT NULL,\n" +
                "  lastname varchar(45) DEFAULT NULL\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=UTF8";

        boolean result = dataDefinitionDAO.execute(sentence);

        assertThat(result, is(true));
    }
}
