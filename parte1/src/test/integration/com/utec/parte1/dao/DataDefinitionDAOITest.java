package com.utec.parte1.dao;

import com.utec.parte1.exception.DataDefinitionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("stage")
public class DataDefinitionDAOITest {


    @Autowired
    private DataDefinitionDAO dataDefinitionDAO;

    @Test
    public void givenASqlString_ThenCreateATable() {
        String sql = "CREATE TABLE `user` (\n" +
                "  `id` int(11) DEFAULT NULL,\n" +
                "  `name` varchar(45) DEFAULT NULL,\n" +
                "  `lastname` varchar(45) DEFAULT NULL\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=latin1";

        boolean result = dataDefinitionDAO.execute(sql);

        assertThat(result, is(true));
    }

    @Test(expected = DataDefinitionException.class)
    public void givenAnInvalidSqlString_ThenThrowAnException() {
        String sql = "CREATE TABsdLE `user` (\n" +
                "  `id` int(11) DEFAULT NULL,\n" +
                "  `name` varchar(45) DEFAULT NULL,\n" +
                "  `lastname` varchar(45) DEFAULT NULL\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=latin1";

        dataDefinitionDAO.execute(sql);
    }
}
