package com.utec.parte1.service;

import com.utec.parte1.bean.BodyRS;
import com.utec.parte1.dao.DataDefinitionDAO;
import org.springframework.stereotype.Service;

@Service
public class DDLService {

    private DataDefinitionDAO dataDefinitionDAO;

    public DDLService(DataDefinitionDAO dataDefinitionDAO) {
        this.dataDefinitionDAO = dataDefinitionDAO;
    }

    public BodyRS process(String body) {
        dataDefinitionDAO.execute(body);
        return new BodyRS("200", "OK");
    }
}
