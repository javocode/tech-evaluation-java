package com.utec.parte1.exception;

public class DataDefinitionException extends RuntimeException {

    public DataDefinitionException(Throwable cause) {
        super(cause);
    }

    public DataDefinitionException(String message) {
        super(message);
    }

    public DataDefinitionException(String message, Throwable cause) {
        super(message, cause);
    }
}
