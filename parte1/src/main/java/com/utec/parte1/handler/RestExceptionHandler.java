package com.utec.parte1.handler;

import com.utec.parte1.bean.BodyRS;
import com.utec.parte1.exception.DataDefinitionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DataDefinitionException.class)
    public final ResponseEntity<Object> handleSaleException(Exception ex, WebRequest request) {

        return buildExceptionResponseEntity(ex);
    }

    private ResponseEntity<Object> buildExceptionResponseEntity(Exception ex) {
        BodyRS bodyRS = new BodyRS("500", String.format("Error: %s", ex.getMessage()));
        return new ResponseEntity<>(bodyRS, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
