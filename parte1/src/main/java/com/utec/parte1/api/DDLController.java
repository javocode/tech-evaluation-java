package com.utec.parte1.api;

import com.utec.parte1.service.DDLService;
import com.utec.parte1.bean.BodyRS;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(DDLController.BASE_PATH)
public class DDLController {

    static final String BASE_PATH = "db-util-api";
    private DDLService ddlService;

    public DDLController(DDLService ddlService) {
        this.ddlService = ddlService;
    }

    @PostMapping(value = "/ddl",
            consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BodyRS process(@RequestBody String body) {

        return ddlService.process(body);
    }
}
