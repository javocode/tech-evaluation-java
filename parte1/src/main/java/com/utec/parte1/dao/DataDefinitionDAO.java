package com.utec.parte1.dao;

import com.utec.parte1.exception.DataDefinitionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DataDefinitionDAO {

    private JdbcTemplate jdbcTemplate;

    public DataDefinitionDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean execute(String sentence) {
        try {
            jdbcTemplate.execute(sentence);
        } catch (Exception e) {
            throw new DataDefinitionException(e);
        }
        return true;
    }
}
