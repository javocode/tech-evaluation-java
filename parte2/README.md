# Pasos para ejecutar la Aplicación

* Crear un archivo .xml con el sgt contenido

```xml
<?xml version="1.0"?>
<tests>
   <test id="loginEndpoint" name="login endpoint">
      <description>endpoint para loguearse</description>
      <url>http://localhost:5000/login</url>
      <method>GET</method>
      </body>
      <parameters>
        <parameter key="user">jon</parameter>
        <parameter key="password">doe</parameter>
      </parameters>
   </test>
   <test id="101" name="get user info">
      <description>regresa la info del usuario</description>
      <url>http://localhost:5000/info</url>
      <method>GET</method>
      </body>
      <parameters>
        <parameter key="token">$loginEndpointResponse.token</parameter>
      </parameters>
   </test>
</tests>
```

* Copiar la ruta donde se encuentra el archivo en la propiedad application.properties (parte2.filePath)
* Ejecutar la aplicación con el comando mvn spring-boot:run