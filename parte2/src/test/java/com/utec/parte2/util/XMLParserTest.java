package com.utec.parte2.util;

import com.utec.parte2.config.TestEndpoints;
import com.utec.parte2.core.Futbolista;
import com.utec.parte2.core.Seleccion;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class XMLParserTest {

    @Test
    void parseXMLToObjectFromFile() {
        String file = Objects.requireNonNull(getClass().getClassLoader()
                .getResource("file.xml")).getFile();

        TestEndpoints testEndpoints = (TestEndpoints) XMLParser.parse(file, TestEndpoints.class);

        assertThat(testEndpoints, is(notNullValue()));
    }

    @Test
    void name() {

    }
}
