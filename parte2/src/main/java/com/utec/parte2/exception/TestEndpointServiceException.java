package com.utec.parte2.exception;

public class TestEndpointServiceException extends RuntimeException {

    public TestEndpointServiceException(String message) {
        super(message);
    }

    public TestEndpointServiceException(Throwable cause) {
        super(cause);
    }
}
