package com.utec.parte2.util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

public final class XMLParser {
    private XMLParser() {
    }
    public static Object parse(String file, Class type) {
        JAXBContext jaxbContext;
        JAXBElement<?> root = null;
        try {
            jaxbContext = JAXBContext.newInstance(type);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            root = jaxbUnmarshaller.unmarshal(new StreamSource(file), type);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return root.getValue();
    }
}
