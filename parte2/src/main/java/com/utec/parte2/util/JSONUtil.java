package com.utec.parte2.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utec.parte2.core.Token;

public final class JSONUtil {

    private JSONUtil() {
    }

    public static Token parseToObject(String json) {
        try {
            return new ObjectMapper().readValue(json, Token.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
