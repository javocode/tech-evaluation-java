package com.utec.parte2;

import com.utec.parte2.config.FileProperties;
import com.utec.parte2.core.TestEndpointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Parte2Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Parte2Application.class, args);
    }

    @Autowired
    private FileProperties fileProperties;
    @Autowired
    private TestEndpointService testEndpointService;

    @Override
    public void run(String... args) {
        testEndpointService.process(fileProperties);
    }
}
