package com.utec.parte2.core;

import com.utec.parte2.config.FileProperties;
import com.utec.parte2.config.Parameter;
import com.utec.parte2.config.TestEndpoint;
import com.utec.parte2.config.TestEndpoints;
import com.utec.parte2.exception.TestEndpointServiceException;
import com.utec.parte2.util.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
public class TestEndpointService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestEndpointService.class);
    private RestTemplate restTemplate;

    public TestEndpointService() {
        this.restTemplate = new RestTemplate();
    }

    public void process(FileProperties fileProperties) {
        TestEndpoints testEndpoints = fileProperties.getTestEndpoints();
        List<TestEndpoint> tests = testEndpoints.getEndpoints();
        Futbolista f = new Futbolista();

        Token token = null;
        for (TestEndpoint test : tests) {
            if ("loginEndpoint".equalsIgnoreCase(test.getId())) {
                String json = handleExecute(test);
                token = JSONUtil.parseToObject(json);
            } else {
                if (token != null) {
                    for (Parameter parameter : test.getParameters().getParameters()) {
                        if ("token".equals(parameter.getKey())) {
                            parameter.setValue(token.getToken());
                        }
                    }
                }
                handleExecute(test);
            }
        }
    }

    private String handleExecute(TestEndpoint test) {
        try {
            String response = execute(test);
            LOGGER.info(String.format("<<< Response: %s", response));
            return response;
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage());
        }
        return "";
    }

    private String execute(TestEndpoint testEndpoint) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(testEndpoint.getUrl());

        for (Parameter parameter : testEndpoint.getParameters().getParameters()) {
            builder.queryParam(parameter.getKey(), parameter.getValue());
        }

        ResponseEntity<String> responseEntity;
        String fullUrl = builder.toUriString();
        try {
            HttpMethod httpMethod = buildMethod(testEndpoint.getMethod());
            responseEntity = restTemplate.exchange(fullUrl, httpMethod, null, String.class);
        } catch (Exception e) {
            throw new TestEndpointServiceException(e);
        }
        if (responseEntity.getBody() == null) {
            throw new TestEndpointServiceException("The body is empty");
        }
        if (responseEntity.getStatusCode().is4xxClientError()) {
            throw new TestEndpointServiceException("There was an error from client");
        }
        return responseEntity.getBody();
    }

    private HttpMethod buildMethod(String method) {
        switch (method) {
            case "GET":
                return HttpMethod.GET;
            default:
                throw new TestEndpointServiceException("Method not supported: " + method);
        }
    }
}
