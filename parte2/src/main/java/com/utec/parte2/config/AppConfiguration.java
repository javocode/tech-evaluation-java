package com.utec.parte2.config;

import com.utec.parte2.util.XMLParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Value("${parte2.filePath}")
    private String filePath;

    @Bean("billingProperties")
    public FileProperties getBillingProperties() {
        FileProperties fileProperties = new FileProperties();
        TestEndpoints testEndpoints = (TestEndpoints) XMLParser.parse(filePath, TestEndpoints.class);
        fileProperties.setTestEndpoints(testEndpoints);
        return fileProperties;
    }
}
