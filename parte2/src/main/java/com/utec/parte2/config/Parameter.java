package com.utec.parte2.config;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "parameter", propOrder = {
        "value"
})
public class Parameter {
    @XmlValue
    private String value;
    @XmlAttribute(name = "key", required = true)
    private String testKey;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return testKey;
    }

    public void setKey(String key) {
        this.testKey = key;
    }
}
