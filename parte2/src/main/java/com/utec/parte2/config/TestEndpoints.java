package com.utec.parte2.config;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "tests")
public class TestEndpoints {
    @XmlElement(name = "test", required = true)
    private List<TestEndpoint> tests;

    public List<TestEndpoint> getEndpoints() {
        return tests;
    }

    public void setEndpoints(List<TestEndpoint> endpoints) {
        this.tests = endpoints;
    }
}
