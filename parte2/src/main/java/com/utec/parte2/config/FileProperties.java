package com.utec.parte2.config;

public class FileProperties {

    private TestEndpoints testEndpoints;

    public TestEndpoints getTestEndpoints() {
        return testEndpoints;
    }

    public void setTestEndpoints(TestEndpoints testEndpoints) {
        this.testEndpoints = testEndpoints;
    }
}
