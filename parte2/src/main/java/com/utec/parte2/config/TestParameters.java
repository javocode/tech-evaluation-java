package com.utec.parte2.config;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class TestParameters {
    @XmlElement(name = "parameter", required = true)
    private List<Parameter> testParameter;

    public List<Parameter> getParameters() {
        return testParameter;
    }

    public void setParameters(List<Parameter> parameters) {
        this.testParameter = parameters;
    }
}
