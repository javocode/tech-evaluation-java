package com.utec.parte2.config;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class TestEndpoint {
    @XmlAttribute(name = "id", required = true)
    private String testId;
    @XmlAttribute(name = "name", required = true)
    private String testName;
    @XmlElement(name = "description", required = true)
    private String testDescription;
    @XmlElement(name = "url", required = true)
    private String testUrl;
    @XmlElement(name = "method", required = true)
    private String testMethod;
    @XmlElement(name = "body", required = true)
    private String testBody;
    @XmlElement(name = "parameters", required = true)
    private TestParameters testParameters;

    public TestParameters getParameters() {
        return testParameters;
    }

    public void setTestParameters(TestParameters testParameters) {
        this.testParameters = testParameters;
    }

    public String getId() {
        return testId;
    }

    public void setId(String id) {
        this.testId = id;
    }

    public String getName() {
        return testName;
    }

    public void setName(String name) {
        this.testName = name;
    }

    public String getDescription() {
        return testDescription;
    }

    public void setDescription(String description) {
        this.testDescription = description;
    }

    public String getUrl() {
        return testUrl;
    }

    public void setUrl(String url) {
        this.testUrl = url;
    }

    public String getMethod() {
        return testMethod;
    }

    public void setMethod(String method) {
        this.testMethod = method;
    }

    public String getBody() {
        return testBody;
    }

    public void setBody(String body) {
        this.testBody = body;
    }
}
